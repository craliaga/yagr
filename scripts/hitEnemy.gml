/// Collision entre el arma y el enemigo
    // ScWriteToLog(0, argument0 );
    with(argument0){
        // ScWriteToLog(0, object_get_name(object_index) + " -> " + 
        // string(CalculaDanho(argument0)));
        hp = hp - CalculaDanho(argument0);
        if(hp <= 0) { // Si ya no tiene energia se destruye
            // Buscar cuantas instancias de este generador hay en el mapa
            instances = ds_map_find_value(global.mapaSpawn, generador); 
            // Decrementamos las instancias
            instances--;
            // Actualizamos el Diccionario de instancias, ahora ya se puede 
            // regenerar una nueva instancia del objeto
            ds_map_replace(global.mapaSpawn, generador, instances);
            // Destruimos el objeto
            instance_destroy();
        }
    }
    // Destruimos el objeto ARMA
    if(argument1!=noone){
        with(argument1) {
            instance_destroy();
        }
    }
    /// ScWriteToLog(type,message)
