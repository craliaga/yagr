// Collision entre el arma y un generador de enemigos
    //ScWriteToLog(0, argument0 );
    with(argument0) {        
        ScWriteToLog(0, object_get_name(object_index) + " -> " + string(CalculaDanho(argument0)));
        //hp = hp - CalculaDanho(global.HeroeElegido);   
        hp = hp - CalculaDanho(argument0);      
        if(hp <= 0) { // Si ya no tiene energia se destruye            
            // Destruimos el generador
            instance_destroy();
        }        
    }    
    // Destruimos el objeto ARMA
    with(argument1) {
        instance_destroy();
    }
