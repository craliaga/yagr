{
    ini_open(working_directory + "/save.ini");
        global.GhostDefensa = ini_read_real("Ghost", "Defensa", 0);
        global.GhostVida    = ini_read_real("Ghost", "Vida", 0);
        global.GhostSpeed   = ini_read_real("Ghost", "Speed", 0);
        global.GhostDamage   = ini_read_real("Ghost", "Damage", 0);
        global.GhostImageSpeed = ini_read_real("Ghost", "ImageSpeed", 1);
        
        global.DemonDefensa = ini_read_real("Demon", "Defensa", 0);
        global.DemonVida    = ini_read_real("Demon", "Vida", 0);
        global.DemonSpeed   = ini_read_real("Demon", "Speed", 0);
        global.DemonDamageMele = ini_read_real("Demon", "DamageMele", 0);
        global.DemonDamageRange = ini_read_real("Demon", "DamageRange", 0);
    
        global.LobberDefensa = ini_read_real("Lobber", "Defensa", 0);
        global.LobberVida    = ini_read_real("Lobber", "Vida", 0);
        global.LobberSpeed   = ini_read_real("Lobber", "Speed", 0);
        global.LoberDamageMele = ini_read_real("Lobber", "DamageMele", 0);
        global.LoberDamageRange = ini_read_real("Lobber", "DamageRange", 0);

        global.GruntDefensa = ini_read_real("Grunt", "Defensa", 0);
        global.GruntVida    = ini_read_real("Grunt", "Vida", 0);
        global.GruntSpeed   = ini_read_real("Grunt", "Speed", 0);
        global.SorcererDefensa = ini_read_real("Sorcerer", "Defensa", 0);
        global.SorcererVida    = ini_read_real("Sorcerer", "Vida", 0);
        global.SorcererSpeed   = ini_read_real("Sorcerer", "Speed", 0);
        global.DeathDefensa = ini_read_real("Death", "Defensa", 0);
        global.DeathVida    = ini_read_real("Death", "Vida", 0);
        global.DeathSpeed   = ini_read_real("Death", "Speed", 0);
    ini_close();
}
