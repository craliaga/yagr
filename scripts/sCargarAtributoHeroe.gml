{
    ini_open(working_directory + "/save.ini");
        global.Speed = ini_read_real(argument0, "Speed", 2.5);
        global.ShotSpeed = ini_read_real(argument0, "ShotSpeed", 15);
        global.ShotPower = ini_read_real(argument0, "ShotPower", 1);
        global.MagicPower = ini_read_real(argument0, "MagicPower", 15);
        global.Armor = ini_read_real(argument0, "Armor", 15);
        
        global.Salud = ini_read_real(argument0, "Salud", 550);
        global.Hambre = ini_read_real(argument0, "Hambre", 100);
        global.Magia = ini_read_real(argument0, "Magia", 35);
    ini_close();
}
