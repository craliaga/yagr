// Arg0 es el objeto enemigo (el que ataca), ataque segun el daño que tenga el enemigo
// = (((($K$14 * $K$14) / ($K$14 + C4 )) + $J$6) - $B$11 + $B$1) * $B$2   
    baseDamage1 = argument0.damage;
    var critico = 1;
    if(scrRoll_Dice(1,100) > ( 100 - global.level) ){   // Cada lvl mas posibilidades de golpe crítico
        critico = 2;
    }
    var damage = ((((baseDamage1 * baseDamage1) / (baseDamage1 + oPlayer.Armor)) 
    + argument0.hp) + global.level - oPlayer.extraArmor ) * critico;
    var mensaje = "";
    if(critico = 2){
        with(argument0)
            mensaje = object_get_name(object_index) + " " + string(damage) + " -> CRITICO!!!!";
    }else{
        with(argument0)
            mensaje = object_get_name(object_index) + " " + string(damage) + " -> Ataque normal";
    }
    ScWriteToLog(0, mensaje);
    
    oPlayer.Salud = oPlayer.Salud - damage;
    //var danho = random(ceil(argument0.ataque))+1;
    sDmgLog(oDmgLog, damage);
