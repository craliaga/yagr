    // Arg0 es el objeto Log
    // Arg1 es el valor del daño
     with (argument0) {
        for (i = length - 1; i >= 0; i -= 1) 
            text[i + 1] = text[i]
        length = min(maxlength, length + 1);
        text[0] = string(argument1);        
    } 
