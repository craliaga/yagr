{
    ini_open(working_directory + "/save.ini");
        global.DebugAreaVision = ini_read_string("Debug", "AreaVision", "N");
        global.DebugLineaVision = ini_read_string("Debug", "LineaVision", "N");
        global.RecuadroGenerador = ini_read_string("Debug", "RecuadroGenerador", "N");
        global.BarraVida = ini_read_string("Performance", "BarraVida", "N");
        global.Sombra = ini_read_string("Performance", "Sombra", "N");
        global.Sonido = ini_read_string("Performance", "Sonido", "S");
        global.ShowHurt = ini_read_string("Debug", "ShowHurt", "S");
        global.Disparo=270; //??? mirando al sur?
        
        
        global.MaxEnemigosPantalla = ini_read_real("Macros", "MaxEnemigosPantalla", "40");
        global.MaxDistancia = ini_read_real("Macros", "MaxDistancia", "300");
        global.MaxInstances = ini_read_real("Macros", "MaxInstances", "8");
        global.SpawnGhostRatio = ini_read_real("Macros", "SpawnGhostRatio", "45");
        global.SpawnDemonRatio = ini_read_real("Macros", "SpawnDemonRatio", "45");
        global.SpawnLobberRatio = ini_read_real("Macros", "SpawnLobberRatio", "45");
        global.DistanciaAtaque  = ini_read_real("Macros","DistanciaAtaque",30);     
        global.DistanciaPersigue= ini_read_real("Macros","DistanciaPersigue",70);   
        global.DistanciaAtento  = ini_read_real("Macros","DistanciaAtento",100); 
        global.DistanciaNormal  = ini_read_real("Macros","DistanciaNormal",200);
    ini_close();
}
