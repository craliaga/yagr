/// Dibuja un texto con sombra

draw_set_font(argument0);
draw_set_color(c_black);
draw_text(argument1, argument2, argument3);       
draw_set_color(c_white);
draw_text(argument1 + 1, argument2, argument3); 
