/// Determinamos el tipo de objeto 
{
    switch(argument1.object_index){
        case oPocion:
        if(sPonPocionEnInventario()) {
            with(argument1) {
                instance_destroy();
            }
        }
        break;
        case oLlave:
            if(sPoneLlaveInventario()) {
                with(argument1){
                    instance_destroy();
                    instance_create(argument0.x,argument0.y,o100);
                    global.Puntaje += 100;
                }
            }
        break;
        case oTesoro:
            with(argument1){
                instance_destroy();
                instance_create(argument0.x,argument0.y, o100);
                global.Puntaje += 100;
            }
        break;
        case oComida:
            switch(argument1.TipoComida){
                case 0:
                case 3:
                case 4:
                case 5:
                    oPlayer.Salud += 100;
                    instance_create(argument0.x,argument0.y, o100);
                break;
                case 1:
                    oPlayer.Salud -= 100;
                    instance_create(argument0.x,argument0.y, ol100);
                break;
                case 2:
                    randomize();
                    r = choose(0,1);
                    if(r=0){
                        oPlayer.Salud += 100;
                        instance_create(argument0.x,argument0.y, o100);
                    }
                    else{
                        oPlayer.Salud -= 100;
                        instance_create(argument0.x,argument0.y, ol100);
                    }
                break;
            }
            with(argument1){
                instance_destroy();
            }
        break;
        
        
    }
}
