/// Devuelve el estado del enemigo segun el Player
// Parameter0 = enemigo
with(argument0) {
    if distance_to_object(argument1) >= global.DistanciaNormal {
            return states.normal;
        }
    if distance_to_object(argument1) < global.DistanciaNormal and
        distance_to_object(argument1) >= global.DistanciaAtento{
            return states.atento;
        }
    if distance_to_object(argument1) < global.DistanciaAtento and
        distance_to_object(argument1) >= global.DistanciaPersigue {
            return states.persigue;
        }
    if distance_to_object(argument1) < global.DistanciaPersigue and
        distance_to_object(argument1) >= global.DistanciaAtaque {
            return states.persigue;
        }
    if distance_to_object(argument1) <= global.DistanciaAtaque {
            return states.ataca;
        }
    return states.normal;
}
