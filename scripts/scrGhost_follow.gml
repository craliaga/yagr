/// scr_GhostFollow
    if(!collision_line(x,y,oPlayer.x,oPlayer.y,oSolido,true,true)) {            
        inst = instance_nearest(x, y, oPlayer);
        mp_potential_step(inst.x,inst.y,self.velocidad,false);
    } else {
        ranx = x - 160 + random(320);
        rany = y - 160 + random(320);            
        ranx = median( 0, ranx, room_width - 248 );
        rany = median( 0, rany, room_height );
        //mp_potential_step(ranx,rany,2,1);
        //mp_linear_step_object(oPlayer.x,oPlayer.y, 4, oSolido);
        mp_linear_step(oPlayer.x,oPlayer.y,self.velocidad, 0);
    }
