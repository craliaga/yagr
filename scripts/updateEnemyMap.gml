{
    // Actualiza mapa de enemigos y destruye la instancia
    with(argument0) {
        instance = ds_map_find_value(global.mapaSpawn, generador); 
        instance--;
        ds_map_replace(global.mapaSpawn, generador, instance);        
        instance_destroy();
    }
}
